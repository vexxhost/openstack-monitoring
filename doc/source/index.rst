OpenStack-Ansible Monitoring
============================
``openstack-monitoring`` is a repository that contains a set of playbooks which
deploy ``sensu-client``, configure it to point towards the RabbitMQ cluster
that hosts your ``sensu-server`` and sets up monitoring for an OpenStack
cloud deployed using OpenStack-Ansible.


Usage
-----
The playbooks are quite straightforward to use.  You'll need to clone them
locally on your OpenStack-Ansible deploy node as they rely on the inventory
structure used by OpenStack-Ansible to work.

#. Clone the latest version of the ``openstack-monitoring`` Git repository in
   the ``/opt/openstack-monitoring`` directory:

   .. code::

    git clone https://opendev.org/vexxhost/openstack-monitoring /opt/openstack-monitoring

#. Install the role dependencies

   .. code::

    ansible-galaxy install -r requirements.yml

#. Add the following variables inside your ``user_variables.yml`` file in order
   for the playbooks to ensure that they point towards the RabbitMQ which your
   ``sensu-server`` is configured to listen to.

   .. code::

    sensu_rabbitmq_host:
    sensu_rabbitmq_user:

#. Add the following variables inside your ``user_secrets.yml`` file in order
   for the playbooks to authenticate to your RabbitMQ server (for Sensu) and
   for the monitoring accounts which will be created by the playbooks.

   .. code::

    sensu_rabbitmq_password:
    sensu_monitoring_galera_password:
    sensu_monitoring_rabbitmq_password:

   .. note::

    All of the variables that start with ``sensu_monitoring`` are used for the
    monitoring infrastructure.  The playbooks will create a MySQL user account
    and RabbitMQ user account which will be used for monitoring.

#.  You can choose to run individual playbooks from within the ``playbooks``
    folder or you can opt to run all the playbooks right away by using
    the ``setup-everything.yml`` playbook.

    .. code::

     openstack-ansible setup-everything.yml

    The playbooks are named after the same ones inside OpenStack-Ansible so
    it should be very easy to find which ones that you need to run specifically
    if you need that.


Contributing
------------
This project is hosted inside OpenDev, so it uses Gerrit for code review for
changes.  You can read more about how the process works by checking out
the `Learn the Gerrit Workflow in the Sandbox`_ article.

.. _`Learn the Gerrit Workflow in the Sandbox`: https://docs.openstack.org/infra/manual/sandbox.html
