---
# Copyright 2019 VEXXHOST, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

- name: Monitor rabbitmq
  hosts: "{{ rabbitmq_host_group | default('rabbitmq_all') }}"
  gather_facts: "{{ osa_gather_facts | default(True) }}"
  user: root
  roles:
    - role: sensu_common
  tasks:
    - import_tasks: common-tasks/install-plugins.yml
      vars:
        sensu_plugins: ['sensu-plugins-rabbitmq']

    - name: Get list of all virtual hosts
      changed_when: false
      shell: rabbitmqctl -s list_vhosts
      register: _rabbitmqctl_vhosts
      tags:
        - skip_ansible_lint

    # NOTE(mnaser): rabbitmq_user is currently broken when using newer
    #               versions of RabbitMQ.  This is why it's vendored in
    #               these playbooks.
    #
    #               see: https://github.com/ansible/ansible/issues/48890
    - name: Create monitoring user
      run_once: true
      rabbitmq_user:
        user: sensu
        password: "{{ sensu_monitoring_rabbitmq_password }}"
        tags: monitoring
        permissions: "{{ _rabbitmq_permissions }}"
      vars:
        _rabbitmq_permissions: |-
          {% set permissions = [] %}
          {% for vhost in _rabbitmqctl_vhosts.stdout_lines %}
          {%     set _ = permissions.append({
                   'vhost': vhost,
                   'configure_priv': '.*',
                   'read_priv': '.*',
                   'write_priv': '.*'
                 }) %}
          {% endfor %}
          {{ permissions }}

    - name: Create auth file
      template:
        src: rabbitmq.ini.j2
        dest: /opt/sensu/rabbitmq.ini

    - import_tasks: common-tasks/setup-checks.yml
      vars:
        sensu_checks:
          - name: rabbitmq-alive
            command: "check-rabbitmq-alive.rb -w {{ rabbitmq_management_bind_address }} -i /opt/sensu/rabbitmq.ini"
          - name: rabbitmq-amqp
            command: "check-rabbitmq-amqp-alive.rb -w {{ rabbitmq_management_bind_address }} -i /opt/sensu/rabbitmq.ini"
          - name: rabbitmq-cluster-health
            command: "check-rabbitmq-cluster-health.rb -w {{ rabbitmq_management_bind_address }} -i /opt/sensu/rabbitmq.ini"
          - name: rabbitmq-messages
            command: "check-rabbitmq-messages.rb --host {{ rabbitmq_management_bind_address }} -i /opt/sensu/rabbitmq.ini --warn 100 --critical 250"
          - name: rabbitmq-network-partitions
            command: "check-rabbitmq-network-partitions.rb --host {{ rabbitmq_management_bind_address }} -i /opt/sensu/rabbitmq.ini"
          - name: rabbitmq-node-health
            command: "check-rabbitmq-node-health.rb --host {{ rabbitmq_management_bind_address }} -i /opt/sensu/rabbitmq.ini"
  vars:
    sensu_service: rabbitmq
  environment: "{{ deployment_environment_variables | default({}) }}"
  tags:
    - galera