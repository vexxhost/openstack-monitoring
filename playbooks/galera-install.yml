---
# Copyright 2019 VEXXHOST, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

- name: Monitor galera
  hosts: galera_all
  gather_facts: "{{ osa_gather_facts | default(True) }}"
  user: root
  roles:
    - role: sensu_common
  tasks:
    - name: Install missing libraries
      yum:
        name:
          - MariaDB-devel
          - MariaDB-shared
        state: present

    - import_tasks: common-tasks/install-plugins.yml
      vars:
        sensu_plugins: ['sensu-plugins-mysql', 'sensu-plugins-percona']

    - name: Create monitoring user
      run_once: true
      mysql_user:
        name: sensu
        password: "{{ sensu_monitoring_galera_password }}"
        host: "%"
        priv: 'test.*:ALL/*.*:PROCESS'

    - name: Create .my.cnf file
      template:
        src: my.cnf.j2
        dest: /opt/sensu/.my.cnf

    - import_tasks: common-tasks/setup-checks.yml
      vars:
        sensu_checks:
          - name: mysql-alive
            command: "check-mysql-alive.rb -h{{ galera_wsrep_address }} -i /opt/sensu/.my.cnf"
          - name: mysql-status
            command: "check-mysql-status.rb -h{{ galera_wsrep_address }} -i /opt/sensu/.my.cnf"
          - name: mysql-innodb-lock
            command: "check-mysql-innodb-lock.rb -h{{ galera_wsrep_address }} -i /opt/sensu/.my.cnf"
          - name: mysql-connections
            command: "check-mysql-connections.rb -h{{ galera_wsrep_address }} -i /opt/sensu/.my.cnf -a -w 80 -c 90"
          - name: percona-cluster-size
            command: "check-percona-cluster-size.rb -i /opt/sensu/.my.cnf -e {{ groups['galera_all'] | length }}"
  vars:
    galera_server_id: "{{ inventory_hostname | string_2_int }}"
    galera_wsrep_node_name: "{{ container_name | default(inventory_hostname) }}"
    sensu_service: galera
  environment: "{{ deployment_environment_variables | default({}) }}"
  tags:
    - galera